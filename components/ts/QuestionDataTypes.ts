/// 数据类型验证
export enum QuestionDataTypes {
    dtText= 0,
    dtNumber= 1,
    dtPhone= 2,
}

export default class  DataDefine {
    public static QuestionDataTypes: QuestionDataTypes;

    public TypeName: string = '文本';
    public TypeValue: QuestionDataTypes = QuestionDataTypes.dtText;

    constructor(name: string, value: QuestionDataTypes) {
        this.TypeName = name;
        this.TypeValue = value;
    }
}
export enum QuestionTypes {
   
        qtRadio =0,
        
        qtCheckbox=1,
        
        qtFillBlank=2,
        
        qtSimpleRank=3,
       
        qtSelRank=4,
        
        qtProportion=5,
        
        qtParagraph=6,
        
        qtBigSmallCode=7,
        
        qtOpenQuestion=8,
        
        qtMatrixFillBlank=9,
        
        qtMatrixRadio=10,
       
        qtMatrixCheckBox=11,
        
        qtMatrixJudgeRadio=12,
        
        qtMatrixJudgeCheckbox=13
}

